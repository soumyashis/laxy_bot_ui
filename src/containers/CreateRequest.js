import React, { Component } from "react";

import {FormGroup,FormControl} from 'react-bootstrap';

export default class CreateRequest extends Component {
    constructor(){
        super();
        this.state={
            option:'',
            source:''

        };
        this.handleSourceChange=this.handleSourceChange.bind(this);
        this.handleOptionChange=this.handleOptionChange.bind(this);
    }

    handleSourceChange(e){
        this.setState({source:e.target.value})
    }

    handleOptionChange(e){
        this.setState({option:e.target.value})
    }
    
    
  render() {
    return <div>
       <strong> Create Request</strong>
        <FormGroup controlId="formControlsSelect">
      <FormControl componentClass="select" placeholder="select" onChange={this.handleOptionChange}>
      <option value="">select an option</option>
      <option value="coffee">Go bring me a Coffee</option>
      <option value="water">I'm thirsty</option>
      <option value="document">Send Documents</option>

       </FormControl>
       <FormControl
            type="text"
            value={this.state.source}
            placeholder="Enter source"
            onChange={this.handleSourceChange}
          />
    </FormGroup>
    <a ><img className="col-md-6" src="https://cdn1.medicalnewstoday.com/content/images/articles/321/321371/a-woman-holding-a-cup-of-coffee.jpg"/></a>
    </div>
  }
}


