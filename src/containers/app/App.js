import React, { Component } from "react";
import PropTypes from "prop-types";

/////////////////////////////////////////////////////////////////////////
// BrowserRouter would be preferred over HashRouter, but BrowserRouter
// would require configuring the server. So we will use HashRouter here.
// Please change to BrowserRouter if you have your own backend server.
///////////////////////////////////////////////////////////////////////////
import { HashRouter as Router, Route, Switch } from "react-router-dom";

import { connect } from "react-redux";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import Status from "../login/Login";
import Home from "../home/Home";
import LiveStream from "../about/About";
import NotFound from "../misc/NotFound";

import CreateRequest from '../CreateRequest';
import { logout } from "../../actions/auth";
import {Requests} from '../Requests';

import "./app.css";

class App extends Component {
  handleLogout() {
    const { user } = this.props;
    this.props.dispatch(logout(user));
  }

  render() {
    const { user } = this.props;
    const isAuthenticated = true && user;
    return (
      <Router>
        <div>
          <div className="container">
            <Header user={user} />
            <div className="appContent">
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/liveStream" component={LiveStream} />
                <Route path="/status" component={Status} />
                <Route path="/CreateRequest" component={CreateRequest} />
                <Route path="/requests" component={Requests} />
                <Route component = {NotFound} />
              </Switch>
            </div>
          </div>
          <Footer />
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  user: PropTypes.string,
  dispatch: PropTypes.func.isRequired
};

App.contextTypes = {
  store: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { auth } = state;
  return {
    user: auth ? auth.user : null
  };
};

export default connect(mapStateToProps)(App);
